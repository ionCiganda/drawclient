function googleLogin() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider).then(function () {
        firebase.auth().getRedirectResult().then(function (result) {
            // This gives you a Google Access Token.
            // You can use it to access the Google API.
            var token = result.credential.accessToken;
            // The signed-in user info.
            var user = result.user;
            var user = firebase.auth().currentUser;
            if (user != null) {
                user.providerData.forEach(function (profile) {
                    var myuser = Object.assign(profile);
                    
                    window.localStorage.setItem('myuser', JSON.stringify(myuser));
                    var localStorageUserData = localStorage.getItem('myuser');
                    // Post new User on DB                   
                    app.request.post(serverurl + 'newUser', { user: localStorageUserData }, onSuccess, onError);
                    function onSuccess(data, status) {
                        if (status === 200) {
                            window.localStorage.setItem('myuser', data);
                            view.router.navigate('/home/');
                        }
                    }
                    function onError(err) {
                    }
                });
                return firebase.auth().signInWithEmailAndPassword(email, displayName);
            }
        }).catch(function (error) {       
        });
    });
};
/*LISTENER*/
firebase.auth().onAuthStateChanged(function (user) {   
    if(conection_estateSocket){
        if (user) {           
            socketID = socket.id;
            changeStatus(socketID);
            view.router.navigate('/home/');
            // User is signed in.
        }
        else {
            // User is signed out.
            view.router.navigate('/login/');          
        }
    }
    else{
        view.router.navigate('/splash/');
    }
});
/*SIGN OUT*/
function signout() {
    changeStatus();
    app.panel.close()
    view.router.navigate('/login/');
    firebase.auth().signOut().then(function () {
        window.localStorage.removeItem("myuser");
        // Sign-out successful.
    }).catch(function (error) {
        // An error happened.
    });
};
