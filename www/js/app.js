(function (global) {
	"use strict";
	function onDeviceReady() {
		document.addEventListener("online", onOnline, false);
		document.addEventListener("resume", onResume, false);
		screen.orientation.lock('portrait');
		
		window.plugins.insomnia.keepAwake()
		checkConnection();
		// access current orientation
	}
	function onOnline() {
	}
	function onResume() {
	}
	document.addEventListener("deviceready", onDeviceReady, false);
	document.addEventListener("backbutton", closeAppRemoveMatch)
})(window);

var serverurl = 'https://turbodraw.herokuapp.com/';

var socket = io.connect(serverurl);
// Dom7
var $$ = Dom7;
// General var
var session_id;
var conection_estateSocket = false;
var flagBug = 0;
// Init App
var app = new Framework7({
	id: 'io.framework7.testapp',
	root: '#app',
	routes: routes
});
var view = app.views.create('.view-main', {
	url: '/splash/'
})
var flagDialog = 0
var matchOn = 0
//******************************** */splash
$$(document).on('page:init', '.page[data-name="splash"]', function (e) {
})
//******************************** */services
$$(document).on('page:init', '.page[data-name="services"]', function (e) {
	$$('.buttonServices').on('click', function (e) {
		view.router.navigate('/about/');
	});
})
//******************************** */about
$$(document).on('page:init', '.page[data-name="about"]', function (e) {
	$$('.buttonAbout').on('click', function (e) {
		view.router.navigate('/home/');
	});
})
$$(document).on('page:init', '.page[data-name="login-screen"]', function (e) {
	$$('#googlelogin').on('click', function (e) {
		googleLogin();
	});
});
//********************************** */home
$$(document).on('page:init', '.page[data-name="home"]', function (e) {
	app.dialog.close();
	socketID = socket.id;
	$$('.panel-left').on('panel:open', function () {
		profile("name", "email", "avatar")
	})
	$$('#friends-button').on('click', function (e) {
		view.router.navigate('/friends/')
	});
	$$('#friendprofile-button').on('click', function (e) {
		view.router.navigate('/friendprofile/')
	});
	$$('#addfriends-button').on('click', function (e) {
		view.router.navigate('/addfriends/')
	});
	$$('#privateMatch').on('click', function (e) {
		view.router.navigate('/privatematch/')
	});
	$$('#searchMatch').on('click', function (e) {
		flagDialog = 1
		matchOn = 1
		searchMatch();

	});
	$$('#signout').on('click', function (e) {
		signout();
	});
	$$('#showStats').on('click', function (e) {
		view.router.navigate('/stats/')
	});
	$$('#myProfile').on('click', function (e) {
		view.router.navigate('/myProfile/')
	});
	$$('#request-button').on('click', function (e) {
		view.router.navigate('/friendsRequest/')
	});
	compareUserVersion()
});
$$(document).on('page:afterin', '.page[data-name="home"]', function (e) {
	enableButtonBuscar(socket.id);
	retry();
});
//************************************/palabras
$$(document).on('page:init', '.page[data-name="viewPalabras"]', function () {
	var sessionStorageData = getSessionStorage('data');
	app.request.json(serverurl + 'word', onSuccess, onError);
	var template = $$('#palabrasByProviderScript ').html();
	var compiledTemplate = Template7.compile(template);

	function onSuccess(data, status, xhr) {
		if (status === 200) {
			template7Data = data.words
			var html = compiledTemplate(template7Data);
			$$('#lista-palabras').html(html);
		}
		else {
			genericToast("Error al solicitar palabras", "long", "bottom")
		}
		$$('.buttonWord').on('click', function (e) {
			socket.emit('wordSelected', { word: $$(this).data('word'), data: sessionStorageData, pintor: socket.id, tema: $$(this).data('tema') });
		});
	};
	function onError(xhr, status) {
		genericToast("Error al solicitar palabras", "long", "bottom")
	};
});
//***********************************/ Page Privatematch
$$(document).on('page:init', '.page[data-name="privateMatch"]', function (e) {
	flagDialog = 2
	var matchName;
	var matchPassword;
	var joinName;
	var joinPassword;
	var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
	$$('.popover-creatematch').on('popover:open', function (e, popover) {
		flagDialog = 3
	});
	$$('.popover-joinmatch').on('popover:open', function (e, popover) {
		flagDialog = 3
	});
	$$('.popover-creatematch').on('popover:close', function (e, popover) {
		flagDialog = 2
	});
	$$('.popover-joinmatch').on('popover:close', function (e, popover) {
		flagDialog = 2
	});
	$$('#ok-create-privatematch').on('click', function (e) {
		flagDialog = 1
		matchName = $$('input#createMatchName').val();
		matchPassword = $$('input#createMatchPassword').val();
		app.popover.close();
		app.dialog.preloader('Searching match');
		//Create private match
		app.request.post(serverurl + 'privateMatch', { playerid: localStorageUserData.uid, socketid: socket.id, name: matchName, password: matchPassword, displayName: localStorageUserData.displayName }, onSuccess, onError);
		function onSuccess(data, status, xhr) {
			if (status === 200) {

				window.sessionStorage.setItem('data', data);
			}
		}
		function onError() {
			genericToast("Error al crear la partida", "long", "bottom")
		}
	});
	$$('#ok-join-privatematch').on('click', function (e) {
		flagDialog = 1
		joinName = $$('input#joinMatchName').val();
		joinPassword = $$('input#joinMatchPassword').val();
		app.popover.close();
		//join a private match
		app.request.post(serverurl + 'joinprivate/', { playerid: localStorageUserData.uid, socketid: socket.id, name: joinName, password: joinPassword, displayName: localStorageUserData.displayName }, onSuccess, onError);
		function onSuccess(data, status, xhr) {
			if (status === 200) {
				window.sessionStorage.setItem('data', data);
			}
		}
		function onError() {
			genericToast("Error al unirse a la partida", "long", "bottom")
		}
	});
	$$('#cancel-create-privatematch').on('click', function (e) {
		app.popover.close();
		flagDialog = 2
	});
	$$('#cancel-join-privatematch').on('click', function (e) {
		app.popover.close();
		flagDialog = 2
	});
})
//*****************************/pintor
$$(document).on('page:init', '.page[data-name="pintorPage"]', function (e) {
	flagPintor = false;
	var sessionStorageData = getSessionStorage('data');
	document.getElementById("word").value = sessionStorageData.word;
	//SKETCHPAD
	var sketchpad = new Sketchpad({
		element: '#sketchpad',
		width: 300,
		height: 400,
		penSize: 10,
	});
	//SEND DRAW
	var planos = sketchpad.toObject()
	//progress bar
	showDeterminate(true, 0, planos)
	//Enviamos el dibujo
	$$('#send').on('click', function (e) {
		flagPintor = true;
		sendDraw(planos);
		matchOn = 0
	});
	//Deshacer
	$$('#undo').on('click', function (e) {
		sketchpad.undo();
	});
	// Cambiar tamaño del pincel
	$$('#slider-pincel').change(sizePen)
	function sizePen(event) {
		sketchpad.penSize = $(event.target).val();
	};
	// Brush color change
	$$('#color-picker').on('input', function () {
		sketchpad.color = this.value;
	});
	//Rehacer
	$$('#redo').on('click', function (e) {
		sketchpad.redo();
	});
	//Clear everything Sketchpad    
	$$('#clearSketchpad').on('click', function (e) {
		if (window.confirm("This will remove everything")) {
			sketchpad.clearEverything();
		};
	});
})
//***********************************/ prepartida
$$(document).on('page:init', '.page[data-name="prePartida"]', function () {

	var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
	var sessionStorageData = getSessionStorage('data');
	document.getElementById("player1Input").value = sessionStorageData.player1DisplayName;
	document.getElementById("player2Input").value = sessionStorageData.player2DisplayName;
	$$('#buttonlisto').prop('disabled', false);
	$$('#buttonlisto').removeClass('color-red');
	//Button listo click 
	$$('#buttonlisto').on('click', function (e) {
		var sessionStorageData = getSessionStorage('data');
		socket.emit('lobbyReady', { uuid: localStorageUserData.uid, match: sessionStorageData });
	});
	//--------------- BOTON ATRAS ----------------------
	$$('#atras-prematch').on('click', function (e) {
		socket.emit('lobbyBack', { uuid: localStorageUserData.uid });
	});
});
//******************************* Spinner
$$(document).on('page:init', '.page[data-name="spinnerPage"]', function (e) {
	document.getElementById('content2').className = 'center';
})
//******************************** Visor
$$(document).on('page:init', '.page[data-name="visorPage"]', function (e) {
	flagVisor = false;
	var sketchpad = new Sketchpad({
		element: '#sketchpad2',
		width: 300,
		height: 400,
		penSize: 10,
	});
	//block sketchpad
	sketchpad.readOnly = true;
	if (flagBug == 0) {
		app.dialog.preloader('Selecting word');
	}
	flagBug = 0
	//clear
	$$('#clear').on('click', function (e) {
		document.getElementById('myInput').value = ''

	});
	//keyboard send capture
	document.addEventListener('keydown', function (event) {
		if (event.which == 13) {
			compareWord(0);
		}
	});
	$$('#sendWord').on('click', function (e) {
		compareWord(0);
	});
})
//******************************** myProfile
$$(document).on('page:init', '.page[data-name="myProfile"]', function (e) {
	flagDialog = 2
	document.addEventListener('keydown', function (event) {
		if (event.which == 13) {
			saveName()
		}
	});
	profile("nameP", "emailP", "avatarP")
	//Home Button
	$$('#profileHome').on('click', function (e) {
		view.router.navigate('/home/')
	});
	$$('#profileEdit').on('click', function (e) {
		$$('#nameP').removeAttr('disabled')
		profileShow()
	});
	$$('#profileCancel').on('click', function (e) {
		view.router.refreshPage()
	});
	$$('#profileSave').on('click', function (e) {
		saveName()
	});
	$$('#gallery').on('click', function (e) {
		disablePhoto()
		openCamera()
	});
	$$('#camera').on('click', function (e) {
		disablePhoto()
		openCamera("camera")
	});
})
$$(document).on('page:init', '.page[data-name="friends"]', function (e) {
	flagDialog = 2
	postMyFriends();
	$$('#backHomeFriends').on('click', function (e) {
		view.router.navigate('/home/')
	})
})
$$(document).on('page:init', '.page[data-name="addfriends"]', function (e) {
	flagDialog = 2
	var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
	var activeName = localStorageUserData.displayName;
	var activeUid = localStorageUserData.uid;
	var pasiveUid, pasiveName;
	requestAllUsersListPromise()
		.then(() => {
			$$('.buttonFriend').on('click', function (e) {
				pasiveUid = ($$(this).data('friend-uid'));
				pasiveName = $$(this).data('friend-displayName');
				requestforNewFriend(activeUid, activeName, pasiveUid, pasiveName);
			});
		})
		.catch((error) => {
			genericToast("Error al solicitar la peticion", "long", "bottom")
		})
	var searchbar = app.searchbar.create({
		el: '.searchbar',
		searchContainer: '.list',
		searchIn: '.item-title',
	});
	$$('#agregar-button').on('click', function (e) {
		frienduid = ($$('#frienduuid-input').val());
		friendname = ($$('#friendname-input').val());
		postaddNewFriend(frienduid, friendname)
	})
	$$('#backHomeAddFriends').on('click', function (e) {
		view.router.navigate('/home/')
	})
})
$$(document).on('page:init', '.page[data-name="friendprofile"]', function (e) {
	var dataFriend = JSON.parse(localStorage.getItem('dataFriend'))
	var friendProfileJson = JSON.parse(dataFriend)
	const promiseGetStatsVariables = new Promise((resolve, reject) => {
		resolve(getStatsVariables(friendProfileJson.partidasPintor, friendProfileJson.partidasVisor, friendProfileJson.aciertosPintor, friendProfileJson.aciertosVisor))
	})
	promiseGetStatsVariables.then(res => {

		var pintorPerc = res[0]
		var visorPerc = res[1]
		var labelPitor = res[2]
		var labelVisor = res[3]
		var valorVisor = res[4]
		var valorPintor = res[5]

		var demoGaugeP = app.gauge.create({
			el: '.gauge-pintorFriendProfile',
			type: 'semicircle',
			value: valorPintor,
			size: 250,
			borderColor: '#2196f3',
			borderWidth: 10,
			valueText: pintorPerc,
			valueFontSize: 41,
			valueTextColor: '#2196f3',
			labelText: labelPitor
		});
		var demoGaugeV = app.gauge.create({
			el: '.gauge-visorFriendProfile',
			type: 'semicircle',
			value: valorVisor,
			size: 250,
			borderColor: '#2196f3',
			borderWidth: 10,
			valueText: visorPerc,
			valueFontSize: 41,
			valueTextColor: '#2196f3',
			labelText: labelVisor
		});
	})
	TemplatesWorkInRequest('.friendprofile-template', '#template-friend', dataFriend);
	$$('#backHomeFriendProfile').on('click', function (e) {
		view.router.navigate('/friends/');
	})
})
//Stats page
$$(document).on('page:init', '.page[data-name="statsPage"]', function (e) {
	flagDialog = 2
	var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
	const promiseGetStatsVariables = new Promise((resolve, reject) => {
		resolve(getStatsVariables(localStorageUserData.partidasPintor, localStorageUserData.partidasVisor, localStorageUserData.aciertosPintor, localStorageUserData.aciertosVisor))
	})
	promiseGetStatsVariables.then(res => {

		var pintorPerc = res[0]
		var visorPerc = res[1]
		var labelPitor = res[2]
		var labelVisor = res[3]
		var valorVisor = res[4]
		var valorPintor = res[5]

		var demoGaugeP = app.gauge.create({
			el: '.gauge-pintor',
			type: 'semicircle',
			value: valorPintor,
			size: 250,
			borderColor: '#2196f3',
			borderWidth: 10,
			valueText: pintorPerc,
			valueFontSize: 41,
			valueTextColor: '#2196f3',
			labelText: labelPitor
		});
		var demoGaugeV = app.gauge.create({
			el: '.gauge-visor',
			type: 'semicircle',
			value: valorVisor,
			size: 250,
			borderColor: '#2196f3',
			borderWidth: 10,
			valueText: visorPerc,
			valueFontSize: 41,
			valueTextColor: '#2196f3',
			labelText: labelVisor
		});
	})
	$$('#backHomeStats').on('click', function (e) {
		view.router.navigate('/home/');
	});
})
//Solicitudes de amistad
$$(document).on('page:init', '.page[data-name="friendsRequest"]', function (e) {
	flagDialog = 2
	var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
	var activeUid = localStorageUserData.uid;
	var activeName = localStorageUserData.displayName;

	requestRecivedFriendshipListPromise(activeUid)
		.then(() => {
			$$('.buttonFriend-delete').on('click', function (e) {
				pasiveUid = $$(this).data('uid');
				pasiveName = $$(this).data('name');
				cancelRequest(activeUid, activeName, pasiveUid, pasiveName, pasiveUid)
			});
		})
		.catch((error) => {
			genericToast("Error al gestionar la solicitud", "long", "bottom")
		})
	requestSendedFriendshipListPromise(activeUid)
		.then(() => {
			$$('.buttonFriend-acept').on('click', function (e) {
				frienduid = $$(this).data('uid');
				friendname = $$(this).data('name');
				postaddNewFriend(frienduid, friendname);
			});
			$$('.buttonFriend-delete2').on('click', function (e) {
				pasiveUid = $$(this).data('uid');
				pasiveName = $$(this).data('name');
				cancelRequest(pasiveUid, pasiveName, activeUid, activeName, pasiveUid);
			});
		})
		.catch((error) => {
			genericToast("Error al gestionar la solicitud", "long", "bottom")
		});
	$$('#backHome').on('click', function (e) {
		view.router.navigate('/home/')
	})
});
