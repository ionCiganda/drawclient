////////////////
//HOME//
////////////////
function enableButtonBuscar(iGotUser) {
    if (iGotUser != null) {
        $$('#searchMatch').removeAttr('disabled').removeClass('color-red');
        $$('#privateMatch').removeAttr('disabled').removeClass('color-red');
        return;
    } else {        
        $$('#searchMatch').prop('disabled', true);
        $$('#privateMatch').prop('disabled', true);
    }
}
function closeAppRemoveMatch() {
    if (flagDialog == 1) {       
        app.dialog.close();
        var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
        socket.emit('lobbyBack', { uuid: localStorageUserData.uid });
        enableButtonBuscar(1)
        matchOn = 0
        flagDialog = 0
    }
    else if (flagDialog == 2) {
        app.dialog.close()
        view.router.navigate('/home/')
        flagDialog = 0
    }
    else if (flagDialog == 3) {
        app.popover.close()
        flagDialog = 2
    }
}
// -- ReadyCheck -- //
function readyPlayers(data) {
    
    if (data.listoP1 == true) {
        $$('div#rPlayer1.col-20').html("✓");
    };
    if (data.listoP1 == false) {
        $$('div#rPlayer1.col-20').html("X");
    };
    if (data.listoP2 == true) {
        $$('div#rPlayer2.col-20').html("✓");
    };
    if (data.listoP2 == false) {
        $$('div#rPlayer2.col-20').html("X");
    };
}
function disableButtonListo() {
    
    $$('#buttonlisto').prop('disabled', true);
}
function disableButton(ButtonUbication) {
    $$(ButtonUbication).prop('disabled', true)
}
function enableButton(ButtonUbication) {
    $$(ButtonUbication).prop('disabled', false)
}
////////////////
//PINTOR//
////////////////
var flagPintor = false;
var flagVisor = false;
//PROGRESS BAR
function showDeterminate(inline, rol, planos) {
    var sessionStorageData = getSessionStorage('data');
    
    determinateLoading = true
    if (inline && rol == 0) {
        
        var progressBarEl = app.progressbar.show('#final-determinate-container', 0)
    }
    if (inline && rol == 1) {
        
        var progressBarEl = app.progressbar.show('#final-visor-determinate-container', 0)
    }
    var speed = 100
    progress()
    function progress() {
        if (matchOn == 1) {
            setTimeout(function () {
                var progressBefore = speed
                speed -= 1
                app.progressbar.set(progressBarEl, speed)
                if (progressBefore > -1 && progressBefore < 1) {
                    if (rol == 0 && flagPintor == false) {
                        sendDraw(planos)
                    }
                    if (rol == 1) {
                        compareWord(1)
                    }
                }
                else if (progressBefore > 49 && progressBefore < 51 && rol == 1) {                      
                    genericToast(sessionStorageData.pista, "long", "bottom")
                }
                if (matchOn == 1) {
                    progress()
                }
            }, 300)
        }
    }
}
//send draw
function sendDraw(planos) {
    var sessionStorageData = getSessionStorage('data');
    
    
    
    socket.emit('imagecompleted', { planos: planos, socketid: socket.id, partida: sessionStorageData });
    app.dialog.preloader('Waiting');
};
////////////////
//visor//
////////////////
//tryAgain
function toastTryAgain() {
    window.plugins.toast.showWithOptions(
        {
            message: "Try Again",
            duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
        }
    )
    document.getElementById('myInput').value = ''
}
//function socket validateWord
function socketValidateWord(timeOut) {
    var sessionStorageData = getSessionStorage('data');
    socket.emit('validateWord', { socketid: socket.id, match: sessionStorageData, value: value, timeOut: timeOut })
}
// funcion para comparar palabra
function compareWord(time) {
    
    
    value = $("#myInput").val()
    
    var sessionStorageData = getSessionStorage('data')
    
    var word = sessionStorageData.word
    if (time == 0) {
        if (value.toUpperCase() == word.toUpperCase()) {
            flagVisor = true;
            socketValidateWord(0)
        }
        else {
            toastTryAgain()
        }
    }
    else if (time == 1 && flagVisor == false) {
        document.activeElement.blur();
        $("input").blur();
        if (value.toUpperCase() == word.toUpperCase()) {
            socketValidateWord(0)
        }
        else {
            socketValidateWord(1)
        }
    }
}
function searchMatch() {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    enableButtonBuscar(null);
    
    
    app.request.post(serverurl + 'newmatch', { playerid: localStorageUserData.uid, socketid: socket.id, displayName: localStorageUserData.displayName }, onSuccess, onError);
    function onSuccess(data, status, xhr) {
            app.dialog.preloader('Searching match');
    };
    function onError(xhr, status) {
        genericToast("Error al solicitar partida", "long", "bottom")
    }
}

function getSessionStorage(data) {
    return JSON.parse(sessionStorage.getItem(data));
}

function cambiarButtonListo(data) {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    if ((data.listoP1 && data.player1 == localStorageUserData.uid) || (data.listoP2 && data.player2 == localStorageUserData.uid)) {
        $$('#buttonlisto').addClass('color-red');
        $$('#buttonlisto').text('No Listo');
    } else {
        $$('#buttonlisto').removeClass('color-red');
        $$('#buttonlisto').text('Listo');
    }
}
//CERRAR APP
function close() {
    window.navigator.app.exitApp();
}
//FUNCION PARA CUANDO NO CONECTA EL SOCKET
function retry() {
    time = 10000;
    // sleep time expects milliseconds
    function sleep(time) {
        return new Promise((resolve) => setTimeout(resolve, time));
    }
    // Usage!
    sleep(time).then(() => {
        // Do something after the sleep!
        if (conection_estateSocket == false) {
            app.dialog.create({
                title: 'Error a la conexion de nuestro servidor',
                text: 'Quiere volver a intentarlo?',
                buttons: [
                    {
                        text: 'Salir',
                    },
                    {
                        text: 'Reintentar',
                    }
                ],
                onClick: function (dialog, index) {
                    if (index === 0) {
                        close();
                    }
                    else if (index === 1) {
                        retry();
                    }
                },
            }).open()
        }
    });
}
// Los parametros del toast van entre comillas como si fuese texto
function genericToast(message, duration, position) {
    window.plugins.toast.showWithOptions(
        {
            message: message,
            duration: duration, // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: position,
            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
        }
    )
}
////////////////
//LOGIN//
////////////////
function profile(name, email, avatar) {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    document.getElementById(name).value = localStorageUserData.displayName;
    document.getElementById(email).value = localStorageUserData.email;
    document.getElementById(avatar).src = localStorageUserData.photoURL;
}
////////////////
//MYPROFILE//
////////////////
function profileHidden() {
    document.getElementById('camera').setAttribute("hidden", "true")
    document.getElementById('gallery').setAttribute("hidden", "true")
    document.getElementById('profileCancel').setAttribute("hidden", "true")
    document.getElementById('profileSave').setAttribute("hidden", "true")
    document.getElementById('nameP').setAttribute("disabled", "true")
    $$('#profileEdit').removeAttr('hidden')
}
function profileShow() {
    $$('#camera').removeAttr('hidden')
    $$('#gallery').removeAttr('hidden')
    $$('#profileCancel').removeAttr('hidden')
    $$('#profileSave').removeAttr('hidden')
    document.getElementById('profileEdit').setAttribute('hidden', "true")
}
function disablePhoto() {
    document.getElementById('camera').setAttribute("disabled", "true")
    document.getElementById('gallery').setAttribute("disabled", "true")
}
function saveName() {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var uid = localStorageUserData.uid
    val = $("#nameP").val()
    profileHidden()
    app.request.post(serverurl + 'editName/' + uid, { value: val }, onSuccess, onError);
    function onSuccess(data) {
        localStorage.setItem('myuser', data)
        genericToast("Nombre de usuario actualizado", "short", "bottom");
    }
    function onError(err) {
        `Server error ${err}`
        genericToast("Error al actualizar el nombre de usuario", "short", "bottom");
    }
}

function postMyFriends() {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var uid = localStorageUserData.uid
    app.request.post(serverurl + 'postMyFriends', { user: uid }, onSuccess, onError)
    function onSuccess(data, status) {
        if (status === 200) {
            TemplatesWorkInRequest('.friend-list', '#friendsamigos', data)
        }
        $$('.friendProfileButton').on('click', function (e) {
            var friendUID = $$(this).data('uid');
            
            requestFriendProfile(friendUID);
        });
        $$('.deleteFriend-Button').on('click', function () {
            var frienduid = $$(this).data('uid');
            confirmFriendDelete(frienduid, uid)
        })
    }
    function onError(err) {
        genericToast("Error al cargar los datos de tus amigos", "long", "bottom")
    }
}
//Envia la solicitud de amistad
function requestforNewFriend(activeUid, activeName, pasiveUid, pasiveName) {
    
    
    disableButton(`#${pasiveUid}-button`);
    app.request.post(serverurl + 'requestforNewFriend', { activeUid: activeUid, activeName: activeName, pasiveUid: pasiveUid, pasiveName: pasiveName }, onSuccess, onError)
    function onSuccess(data, status) {
        if (status === 200) {
            deleteElement(`#${pasiveUid}-ff`);
            genericToast("Solicitud enviada", "short", "bottom");
            return;
        } else {
            
            enableButton(`#${pasiveUid}-button`);
        }
    }
    function onError(err) {
        genericToast("Error en la solicitud", "long", "bottom")
    }
}

function confirmFriendDelete(frienduid, uid) {
    
    yes = confirm('¿Estás seguro de que quieres eliminar a este amigo?')
    if (yes == true) {
        requestDeleteFriend(frienduid, uid);
    } else {
        
    }
}
//Añade a amigo
function postaddNewFriend(frienduid, friendname) {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var name = localStorageUserData.displayName;
    var uid = localStorageUserData.uid;
    app.request.post(serverurl + 'addNewFriend', { user: uid, name: name, friendUid: frienduid, friendDisplayName: friendname }, onSuccess, onError)
    function onSuccess(data, status) {
        if (status === 200) {
            
            deleteElement(`#${frienduid}-request`);
            genericToast("Solicitud aceptada", "short", "bottom")
            return;
        } else {
            
        }
    }
    function onError(err) {
        
    }
}
//Promesa para mostar usuarios 
requestAllUsersListPromise = (() => {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var uid = localStorageUserData.uid;
    return new Promise((resolve, reject) => {
        app.request.get(serverurl + 'getAllUsers/' + uid, onSuccess, onError);
        function onSuccess(data, status) {
            if (status === 200) {
                
                TemplatesWorkInRequest('#allUsersList', '#allUsersListInsert', data)
                resolve();
            }
            if (status === 404) {
                genericToast('Tienes que estar logeado', 'long', 'bottom')
            }
        }
        function onError(err) {     
            reject(err);
        }
    })
});
//Promesa para peticiones recibidas
requestRecivedFriendshipListPromise = ((uid) => {
    return new Promise((resolve, reject) => {
        app.request.get(serverurl + 'recivedFriendshipReq/' + uid, onSuccess, onError);
        function onSuccess(data, status) {
            if (status === 200) {
                
                TemplatesWorkInRequest('.friend-recived-request-list', '#lista-requestRecivedFriend', data)
                resolve();
            }
        }
        function onError(err) {
            
            reject(err);
        }
    })
});
//Promesa para peticiones enviadas
requestSendedFriendshipListPromise = ((uid) => {
    return new Promise((resolve, reject) => {
        app.request.get(serverurl + 'sendedFriendshipReq/' + uid, onSuccess, onError);
        function onSuccess(data, status) {
            if (status === 200) {
                
                
                TemplatesWorkInRequest('.friend-sended-request-list', '#lista-requestSendedFriend', data)
                resolve();
            }
        }
        function onError(err) {
            
            reject(err);
        }
    })
});
//Cancelar peticiones
function cancelRequest(activeUid, activeName, pasiveUid, pasiveName, f) {
    
    app.request.post(serverurl + 'cancelFriendshipReq/', { activeUid: activeUid, activeName: activeName, pasiveUid: pasiveUid, pasiveName: pasiveName }, onSuccess, onError)
    function onSuccess(data, status) {
        if (status === 200) {
            
            genericToast("Solicitud rechazada", "short", "bottom");
            deleteElement(`#${f}-request`);
            
            return;
        } else {
            
        }
    }
    function onError(err) {
        genericToast("Error al cancelar la solicitud", "long", "bottom")
    }
}
function requestFriendProfile(friend) {
    app.request.post(serverurl + 'friendProfile', { frienduid: friend }, onSuccess, onError)
    function onSuccess(data, status) {
        if (status === 200) {
            view.router.navigate('/friendprofile/');
            localStorage.setItem('dataFriend', JSON.stringify(data));
        } else {
            
        }
    }
    function onError(err) {
        genericToast("Error al acceder a los datos de este usuario", "long", "bottom")
    }
}

function requestDeleteFriend(frienduuid, myuid) {
    
    app.request.post(serverurl + 'postRemoveFriend/', { frienduid: frienduuid, uid: myuid }, onSuccess, onError)
    function onSuccess(status) {
        if (status == '200') {
            
            deleteElement(`#${frienduuid}-list`);
            genericToast('Amigo eliminado', 'long', 'bottom');
        }
        if (status != '200') {
            
            genericToast('Something might be wrong ', 'long', 'bottom');
        }
    }
    function onError(err) {
        
        genericToast('Something might be wrong ', 'long', 'bottom');
    }
}

function TemplatesWorkInRequest(templateToCompile, insertPathSelector, data) {
    var template = $$(templateToCompile).html();
    var compiletTemplate = Template7.compile(template);
    var dataToJSON = JSON.parse(data)
    var html = compiletTemplate(dataToJSON);
    $$(insertPathSelector).html(html);
}
/* CAMERA */
function setOptions(srcType) {
    var options = {
        // Some common settings are 20, 50, and 100
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: srcType,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: Camera.MediaType.PICTURE,
        allowEdit: true,
        correctOrientation: true
    }
    return options
}

function openCamera(data) {
    if (data == "camera") {
        var srcType = Camera.PictureSourceType.CAMERA
        var options = setOptions(srcType)
    }
    else {
        var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
        var options = setOptions(srcType);
    }
    navigator.camera.getPicture(function cameraSuccess(imageUri) {
        var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
        
        
        if (localStorageUserData.idPhoto == null) {
            
            var imgId = Date.now()
        }
        else {
            
            var imgId = localStorageUserData.idPhoto
        }
        var storageRef = firebase.storage().ref()
        var getFileBlob = function (url, cb) {
            var xhr = new XMLHttpRequest()
            xhr.open("GET", url)
            xhr.responseType = "blob"
            xhr.addEventListener('load', function () {
                cb(xhr.response)
            })
            xhr.send()
        }
        var blobToFile = function (blob, name) {
            blob.lastModifiedDate = new Date();
            blob.name = name;
            return blob;
        }
        var getFileObject = function (filePathOrUrl, cb) {
            getFileBlob(filePathOrUrl, function (blob) {
                cb(blobToFile(blob, imgId + '.jpg'))
            })
        }
        getFileObject(imageUri, function (fileObject) {
            var uploadTask = storageRef.child('images/' + imgId + '.jpeg').put(fileObject)
            uploadTask.on('state_changed', function (snapshot) {
                //Charge progress bar
                $$('#demo-inline-progressbar').removeAttr('hidden')
                var calculoPorcentaje = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                calculoPorcentaje = Math.round(calculoPorcentaje);
                app.progressbar.set('#demo-inline-progressbar', calculoPorcentaje);
            }, function (error) {
                
            }, function () {
                uploadTask.snapshot.ref.getDownloadURL().then(function (result) {
                    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
                    //control url response
                    var url = result
                    document.getElementById('avatarP').src = url
                    //hide profile enable camera and gallery butons
                    document.getElementById('demo-inline-progressbar').setAttribute('hidden', "true")
                    profileHidden()
                    $$('#camera').removeAttr('disabled')
                    $$('#gallery').removeAttr('disabled')
                    //Update DB
                    
                    var uid = localStorageUserData.uid
                    app.request.post(serverurl + 'updatePhoto/' + uid, { photoURL: url, idPhoto: imgId }, onSuccess, onError);
                    function onSuccess(data) {
                        localStorage.setItem('myuser', data)
                        genericToast("Foto actualizada", "short", "bottom")
                    }
                    function onError(err) {
                        genericToast("Error al actualizar la foto de perfil", "short", "bottom")
                            `Server error ${err}`
                    }
                })
            })
        })
    },
        function cameraError(error) {
            genericToast("Error al gestionar la foto", "long", "bottom")
        },
        options)
}

function deleteElement(itemToDelete) {
    $$(itemToDelete).remove();
}
function changeStatus(socketID) {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    if (localStorageUserData) {
        
        
        var uid = localStorageUserData.uid;
        
        app.request.post(serverurl + 'changeStatus', { uid: uid, SocketId: socketID }, onSuccess, onError)
        function onSuccess() {

        }
        function onError(err) {
            
            genericToast('Something might be wrong ', 'long', 'bottom');
        }
    }
    else {
    }
}

function getStatsVariables(partidasPintor, partidasVisor, aciertosPintor, aciertosVisor) {
    
    if (partidasPintor) {
        var valorPintor = aciertosPintor / partidasPintor;
        var labelPitor = aciertosPintor + '/' + partidasPintor + ' correct games'
        var pintorPerc = (valorPintor * 100);// + '%';//'20%'
        pintorPerc = pintorPerc.toFixed(1) + '%';  
    }
    else {
        var valorPintor = 0;
        var pintorPerc = '- %';
        var labelPitor = 'No games';
    }
    if (partidasVisor) {
        var valorVisor = aciertosVisor / partidasVisor;
        var visorPerc = (valorVisor * 100)// + '%';//'40%'
        visorPerc = visorPerc.toFixed(1) + '%';
        var labelVisor = aciertosVisor + '/' + partidasVisor + ' correct games'
    }
    else {
        var valorVisor = 0;
        var visorPerc = '- %';//'40%'
        var labelVisor = 'No games';
    }
    return [pintorPerc, visorPerc, labelPitor, labelVisor, valorVisor, valorPintor];
}
//Function for compare localUserVersion with serverUserVersio
function compareUserVersion() {
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var uid = localStorageUserData.uid;
    var version = localStorageUserData.version;
    app.request.post(serverurl + 'getVersion/', { uid: uid, version: version }, onSuccess, onError);
    function onSuccess(data, status) {
        var valor = JSON.parse(data)
        if (valor.notification == 'notification button change') {
            document.getElementById("request-button").classList.remove('color-blue')
            document.getElementById("request-button").classList.add('color-red')
        }
        else {
            document.getElementById("request-button").classList.remove('color-red')
            document.getElementById("request-button").classList.add('color-blue')
        }
        if(valor.result){
            localStorage.setItem('myuser', JSON.stringify(valor.result));
        }
    }
    function onError(err) {
    }
}