////////////////
//GENERAL//
////////////////
socket.on('conectado', function (data) {
    conection_estateSocket = true;
    enableButtonBuscar(socket.id);
    var localStorageUserData = localStorage.getItem('myuser');
    if(localStorageUserData){
        view.router.navigate('/home/');
    }
    else{
        view.router.navigate('/login/');
    }
    enableButtonBuscar(data.user);
});
//Cuando el otro player se desconecta
socket.on('conectionLost', function (data) {
    if (socket.id == data) { 
        window.plugins.toast.showWithOptions(
            {
                message: "Tu oponente se a desconectado",
                duration: "short", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
                position: "bottom",
                addPixelsY: -40  // added a negative value to move it up a bit (default 0)
            }
        );
        matchOn = 0;
        app.dialog.close();
        view.router.navigate('/home/');
    }
});
///////////////////////
//PRE-PARITIDA//
///////////////////////
socket.on('navigate', function (data) {
    view.router.navigate(data.navigate);
    readyPlayers(data)
});
socket.on('ticks', function (data) {
    window.sessionStorage.setItem('data', JSON.stringify(data))
    var localStorageUserData = JSON.parse(localStorage.getItem('myuser'));
    var listo1 = data.listoP1;
    var listo2 = data.listoP2;
    var Uuid = localStorageUserData.uid;
    readyPlayers(data);
    cambiarButtonListo(data)
});
// --- PLAYER LEFT -- //
socket.on('playerLeft', function (data) {
    app.dialog.close()
    view.router.navigate(data.navigate)
})
// Enable button  Listo when match is full

socket.on('matchfull', function (data) {
    flagDialog = 1
    matchOn = 1
    app.dialog.close();
    window.sessionStorage.setItem('data', JSON.stringify(data.result));
    view.router.navigate(data.navigate)
})
////////////////
//palabra//
////////////////
socket.on('wordUpdated', function (data) {
    
    window.sessionStorage.setItem('data', JSON.stringify(data.partida));
    if (data.navigate == '/visor/') {
        app.dialog.close();
        app.dialog.preloader('Drawing');
    }
    else if (data.navigate == '/pintor/') {
        view.router.navigate(data.navigate)
    }
})
////////////////
//VISOR//
////////////////

socket.on('sendDibujo', function (data) {
    
    app.dialog.close();
    //progress bar
    showDeterminate(true, 1)
    data.planos.element = '#sketchpad2'
    var sketchpad = new Sketchpad(data.planos)
    sketchpad.animate(30)
});
// validacion
socket.on('correctWord', function (data) {
    matchOn = 0
    flagDialog = 2
    window.localStorage.setItem('myuser', JSON.stringify(data.player));
    app.dialog.close();
    app.dialog.create({
        title: data.message,
        text: 'Play again?',
        buttons: [
            {
                text: 'Yes',
            },
            {
                text: 'No',
            }
        ],
        onClick: function (dialog, index) {
            if (index === 0) {
                flagDialog = 1
                matchOn = 1
                searchMatch()
            }
            else if (index === 1) {
                //Button no clicked
                flagBug = 1
                view.router.navigate('/home/')
            }
        },
    }).open()
})
//Partida privada no encontrada 

socket.on('sadsmiley', function () {
    flagDialog = 2  
    window.plugins.toast.showWithOptions(
        {
            message: "No hay ninguna partida con estas credenciales",
            duration: "long", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
        }
    );
})
//Cuando la conexion con el server es OK
socket.on('newFriendRequest', function (data) {  
    // Create full-layout notification
    var notificationFriendRequest = app.notification.create({
        icon: '<i class="icon f7-icons">person</i>',
        title: 'New Friend Request',
        titleRightText: 'now',
        //subtitle: 'This is a subtitle',
        text: data.activeName + ' wants to be your friend',
        closeTimeout: 5000,
    })
    notificationFriendRequest.open();
    compareUserVersion()
})
