function checkConnection() {
    var networkState = navigator.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';
};
//OFFLINE
document.addEventListener("offline", onOffline, false);
function onOffline() {  
    // Handle the offline event
    genericToast('El jugador ha perdido la conexion a la red','short','bottom');    
    conection_estateSocket = false; 
    matchOn = 0;
    app.dialog.close();      
    view.router.navigate('/splash/');
};
