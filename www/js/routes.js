var routes = [
  // Index page
  {
    path: '/',
    url: './index.html',
    name: 'home',
  },
   // Splash page
   {
    path: '/splash/',
    url: './pages/splash.html',
    name: 'splash',
  },
  // Home page
  {
    path: '/home/',
    url: './pages/home.html',
    name: 'home',
  },
  // About page
  {
    path: '/about/',
    url: './pages/about.html',
    name: 'about',
  },
  //private match
  {
    path: '/privatematch/',
    url: './pages/privatematch.html',
    name: 'privatematch',
  },
  //Word selection page
  {
    path: '/palabra/',
    url: './pages/palabra.html',
    name: 'palabra',
  },
  // Draw page
  {
    path: '/pintor/',
    url: './pages/pintor.html',
    name: 'pintor',
  },
  //Pre partida
  {
    path: '/prematch/',
    url: './pages/prematch.html',
    name: 'prematch',
  },
  // Viewer page
  {
    path: '/visor/',
    url: './pages/visor.html',
    name: 'visor',
  },
  // Stats page
  {
    path: '/stats/',
    url: './pages/stats.html',
    name: 'stats',
  },
  // myProfile page
  {
    path: '/myProfile/',
    url: './pages/myProfile.html',
    name: 'myProfile',
  },
  {
    path:'/friends/',
    url:'./pages/friends.html',
    name:'friends',
  },
  {
    path:'/friendprofile/',
    url:'./pages/friendprofile.html',
    name:'friendprofile',
  },
  {
    path:'/addfriends/',
    url:'./pages/addfriends.html',
    name:'addfriends',
  },
  //friendsRequest page
  {
    path:'/friendsRequest/',
    url:'./pages/friendsRequest.html',
    name:'friendsRequest',
  }, 
  {
    path:'/login/',
    url:'./pages/login.html',
    name:'login',
  },     
];
